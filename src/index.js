import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ImageEditor from './ImageEditor';

ReactDOM.render(<ImageEditor />, document.getElementById('root'));
