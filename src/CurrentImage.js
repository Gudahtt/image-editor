import React, { Component } from 'react';

class CurrentImage extends Component {
  render() {
    if (!this.props.currentImage) {
      return (
        <div className="current-image">
          No image selected
        </div>
      );
    }

    return (
      <div className="current-image">
        
        <img src={this.props.currentImage} alt="" />
      </div>
    );
  }
}

export default CurrentImage;
