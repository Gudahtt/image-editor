import React, { Component } from 'react';

class Controls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: ''
    };
  }

  handleChange(event) {
    this.setState({
      url: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSelectImage(this.state.url);
  }

  render() {
    return (
      <div className="controls">
          <div className="select-image">
            <form onSubmit={(e) => this.handleSubmit(e)}>
                <label>
                    URL:
                    <input value={this.state.url} onChange={(e) => this.handleChange(e)} />
                </label>
                <input type="submit" value="Select" />
            </form>
          </div>
          <div className="operations">
            <button onClick={() => this.props.onCrop()}>Crop</button>
          </div>
      </div>
    );
  }
}

export default Controls;
