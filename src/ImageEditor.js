import React, { Component } from 'react';

import Controls from './Controls';
import CurrentImage from './CurrentImage';

const API_URL = "http://localhost:8000/api/"

class ImageEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: null
    };
  }

  handleCrop() {
    const params = new URLSearchParams();
    params.set('url', this.state.currentImage);
    params.set('width', 100);
    params.set('height', 100);

    const cropUrl = API_URL + 'crop?' + params.toString();

    fetch(cropUrl, {
      method: 'POST'
    })
      .then((response) => {
        if (response.status !== 200) {
          return Promise.reject(new Error(response.statusText));
        }

        return response.json();
      })
      .then((json) => {
          this.setState({ currentImage: json.url });
      })
      .catch((e) => {
        console.error(e);
      })
  }

  render() {
    return (
      <div className="image-editor">
        <header>
          <h1>Image Editor</h1>
        </header>
        <Controls onSelectImage={image => this.setState({ currentImage: image })} onCrop={() => this.handleCrop()} />
        <CurrentImage currentImage={this.state.currentImage} />
      </div>
    );
  }
}

export default ImageEditor;
